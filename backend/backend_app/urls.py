from django.urls import path,include
from rest_framework import routers

from backend_app.rest_views import UserViewSet,ProductViewset,OrdersViewset,OrderItemViewset
from rest_framework.authtoken.views import obtain_auth_token

router = routers.DefaultRouter()
router.register('account/register', UserViewSet,)
router.register('rest/products',ProductViewset)
router.register('rest/orders',OrdersViewset,basename="Orders")
router.register('rest/orderitems',OrderItemViewset)
# router.register('register', UserViewSet)

urlpatterns=[
    path('',include(router.urls)),
    path('account/login/',obtain_auth_token)
]
