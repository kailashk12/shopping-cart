from django.contrib.auth.models import User
from django.db import models

# Create your models here.

class Products(models.Model):
    title=models.CharField(max_length=30)
    description = models.TextField(max_length=100)
    image=models.ImageField(upload_to='templates/images')
    price=models.IntegerField()
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    def __str__(self):
        return f'{self.title}'





class Orders(models.Model):
    payment_references = (
        ("upi", "Upi"),
        ("paypal", "Paypal"),
        ("creditcard", "Creditcard"),
        ("paylater", "Paylater"),

    )
    status_references=(
        ('new','New'),
        ('paid','Paid')
    )

    user_id=models.ForeignKey(User,on_delete=models.CASCADE)
    total =models.IntegerField()
    products = models.ManyToManyField(Products)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)
    status= models.CharField(max_length=20,choices=status_references)
    mode_of_payment=models.CharField(max_length=20,choices=payment_references)



    def __str__(self):
        return f"{self.id}"

class OrdersItems(models.Model):
    user_id=models.ForeignKey(User,on_delete=models.CASCADE,default=None)

    # order_id=models.ForeignKey(Orders,on_delete=models.CASCADE)
    product_id=models.ForeignKey(Products,on_delete=models.CASCADE)
    Quantity=models.IntegerField()
    # price=models.IntegerField()
    # price = Products.objects.filter(product_id__price=Products.price)
    def price(self):
        return self.product_id.price

    def total_cost(self):
        print('product_id price:',self.product_id.price)
        cost=self.Quantity * self.product_id.price
        print('cost:',cost)
        return cost

    def __str__(self):
        return f"{self.product_id}"
    # def total(self):
    #     total=self.Quantity * self.price
