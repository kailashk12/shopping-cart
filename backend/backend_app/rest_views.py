from django.contrib.auth.models import User
from django.http import Http404
from rest_framework.authentication import  TokenAuthentication
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.filters import SearchFilter,OrderingFilter
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from backend_app.models import Orders,Products,OrdersItems
from backend_app.serializers import UserSerializer,ProductSerializer,OrdersSerializer,Order_items_Serializer

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class ProductViewset(viewsets.ModelViewSet):
    queryset = Products.objects.all()
    serializer_class = ProductSerializer
    filter_backends = (SearchFilter, OrderingFilter)
    search_fields = ('id', 'title')





class OrdersViewset(viewsets.ModelViewSet):
    queryset = Orders.objects.all()
    serializer_class = OrdersSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    # count=('-products_count')

    def list(self, request):
        # queryset = User.objects.all()
        orders_data =Orders.objects.filter(user_id = request.user)
        serializer = OrdersSerializer(orders_data, many=True)
        return Response(serializer.data)

    def create(self, request,  *args, **kwargs):
        data = request.data
        # userinstance=User.objects.get(id=2)
        print('user:',request.user)
        # print('username:',userinstance)
        new_order = Orders.objects.create(user_id=request.user,total=data['total'],status=data['status'],mode_of_payment=data['mode_of_payment'])
        new_order.save()
        orders_data = Orders.objects.filter(user_id=request.user)
        print('orders_data:',orders_data)
        for product in data['products']:
            products_data =Products.objects.get(title=product['title'])
            new_order.products.add(products_data)
        serializer = OrdersSerializer(new_order)

        return Response(serializer.data)

    def retrive(self, request, pk=None):
        queryset = Orders.objects.filter(pk=pk,user_id=request.user, )
        if not queryset:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            serializer = OrdersSerializer(queryset)
            return Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        pass


    def partial_update(self, request, pk=None):
        orders= Orders.objects.get(user_id=request.user,pk=pk)
        data = request.data

        try:
            for product in data['products']:
                products_data = Products.objects.get(title=product['title'])
                # products = Products.objects.get(title=data['title'])
                orders.products = products_data
        except Exception:
            print('error',Exception)

        orders.total =data.get('total',orders.total)
        orders.status = data.get('status', orders.status)
        orders.mode_of_payment = data.get('mode_of_payment', orders.mode_of_payment)
        orders.save()

        serialized = OrdersSerializer(request.user, data=request.data, partial=True)
        return Response(serialized.data,status=status.HTTP_202_ACCEPTED)

    # def destroy(self, request, pk=None,*args, **kwargs):
    #     try:
    #         order=Orders.objects.get(user_id=request.user,pk=pk)
    #         instance = self.get_object(order)
    #         self.perform_destroy(instance)
    #     except Http404:
    #         pass
    #     return Response(status=status.HTTP_204_NO_CONT



class OrderItemViewset(viewsets.ModelViewSet):
    queryset = OrdersItems.objects.all()

    serializer_class = Order_items_Serializer
    authentication_classes =  [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    # def list(self, request):
    #     # queryset = User.objects.all()
    #     data = request.data
    #     order_obj =Orders_items.objects.filter(order_id= data['order_id'])
    #     serializer = Order_items_Serializer(order_obj, many=True)
    #     return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        data = request.data
        # user = User.objects.get(username=request.user)
        new_order_item = OrdersItems.objects.get(user_id=request.user,product_id = request.Products,)
        pass
